import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import CountMoviment from './src/countMoviment';
import imageMoviment from './src/imageMoviment';
import Menu from './src/menu';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='Menu'>
        <Stack.Screen name="countMoviment" component={CountMoviment}/>
        <Stack.Screen name="Menu" component={Menu}/>
        <Stack.Screen name="imageMoviment" component={imageMoviment}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
};
