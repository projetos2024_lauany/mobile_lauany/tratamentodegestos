import React, { useRef, useState } from "react";
import { View, Dimensions, PanResponder, Animated, Text } from "react-native";

const CountMoviment = () => {
  const [count, setCount] = useState(0);
  const screenHeight = Dimensions.get("window").height;
  const gestureThreshold = screenHeight * 0.25;

  const fontSize = useRef(new Animated.Value(20)).current;

  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (event, gestureState) => {},
      onPanResponderRelease: (event, gestureState) => {
        if (gestureState.dy < -gestureThreshold) {
          setCount((prevCount) => prevCount + 1);
          // Animação: aumenta o tamanho do texto
          Animated.timing(fontSize, {
            toValue: 10, // Novo tamanho
            duration: 500, // Duração da animação
            useNativeDriver: false, // O Native Driver não é suportado para fonte
          }).start(() => {
            // Resetar o tamanho do texto após a animação
            Animated.timing(fontSize, {
              toValue: 20,
              duration: 0, // Instantâneo
              useNativeDriver: false,
            }).start();
          });
        }
      },
    })
  ).current;

  return (
    <View
      {...panResponder.panHandlers}
      style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
    >
      <Animated.Text style={{ fontSize: fontSize }}>
        contagem: {count}
      </Animated.Text>
    </View>
  );
};

export default CountMoviment;
