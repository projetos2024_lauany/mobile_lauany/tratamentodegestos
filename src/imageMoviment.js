import React, { useRef, useState } from "react";
import {
  View,
  Dimensions,
  PanResponder,
  Image,
  Text,
  StyleSheet,
} from "react-native";

const ImageMoviment = () => {
  const [direction, setDirection] = useState(0);
  const screenWidth = Dimensions.get("window").width;
  const gestureThreshold = screenWidth * 0.25;

  const images = [
    "https://via.placeholder.com/150/FF5733/FFFFFF",
    "https://via.placeholder.com/150/33FF57/FFFFFF",
    "https://via.placeholder.com/150/5733FF/FFFFFF",
  ];

  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderRelease: (event, gestureState) => {
        if (gestureState.dx > 50) {
          setDirection((prevDirection) => {
            if (prevDirection === images.length - 1) {
              return 0;
            } else {
              return prevDirection + 1;
            }
          });
        } else if (gestureState.dx < -50) {
          setDirection((prevDirection) => {
            if (prevDirection === 0) {
              return images.length - 1;
            } else {
              return prevDirection - 1;
            }
          });
        }
      },
    })
  ).current;

  return (
    <View
      {...panResponder.panHandlers}
      style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
    >
      <Image
        source={{ uri: images[direction % images.length] }}
        style={styles.image}
      />
      <Text>IMAGEM:{direction}</Text>
    </View>
  );
};
export default ImageMoviment;

const styles = StyleSheet.create({
  image: {
    width: 150,
    height: 150,
    resizeMode: "contain",
  },
});
