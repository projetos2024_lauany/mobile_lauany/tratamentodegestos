import {

  Text,
  View,
  TouchableOpacity,
} from "react-native";

export default function Menu({ navigation }) {
  return (
    <View>
      <TouchableOpacity onPress={() => navigation.navigate("countMoviment")}>
        <Text>contagem</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate("imageMoviment")}>
        <Text>imagem</Text>
      </TouchableOpacity>
    </View>
  );
} 